FROM python:3-alpine

# Retrieve Flake8_version
ARG FLAKE8_VERSION=6.0.0

RUN pip install flake8==${FLAKE8_VERSION} --no-cache-dir

ENTRYPOINT [ "flake8" ]
