[![MIT License][license-shield]][license-url]

[![LinkedIn][linkedin-shield]][linkedin-url]

  
  

<!-- PROJECT LOGO -->

<br  />

<p  align="center">

<a  href="https://gitlab.com/devsecops-jordan-assouline-tools/flake8-docker-image">

</a>

  

<h3  align="center">Flake8 Docker Image</h3>

  

<p  align="center">

A simple repository to build & store a Flake8 docker image

<br  />

<a  href="https://gitlab.com/devsecops-jordan-assouline-tools/flake8-docker-image"><strong>Explore the docs » </strong></a>

<br  />

<a  href="https://gitlab.com/devsecops-jordan-assouline-tools/flake8-docker-image">View Demo</a> . <a  href="https://gitlab.com/devsecops-jordan-assouline-tools/flake8-docker-image/issues">Report Bug</a> . <a  href="https://gitlab.com/devsecops-jordan-assouline-tools/flake8-docker-image/issues">Request Feature</a>

</p>

</p>

  
  
  

<!-- TABLE OF CONTENTS -->

## Table of Contents

* [About the Project](#about-the-project)
* [Built With](#built-with)
* [Getting Started](#getting-started)
* [Usage](#usage)
* [Roadmap](#roadmap)
* [Contributing](#contributing)
* [License](#license)
* [Contact](#contact)
  

<!-- ABOUT THE PROJECT -->

## About The Project


This a simple project to build a docker image from a Dockerfile into a CI pipeline using Gitlab-CI.
 
Here's why:
* Have a Docker image to run Flake8
* Capability to build a Docker image with the latest Flake8 release
* Images are tested
* Vulnerabilities checks are perform on the images
  
### Built With
* [Python](https://www.python.org/)
* [PIP](https://pypi.org/project/pip/)
* [Flake8](https://flake8.pycqa.org/)

<!-- GETTING STARTED -->

## Getting Started

How to use this image
  
### Usage

1. Pull the image from DockerHub
```sh
docker pull jassouline/flake8:alpine
```

2. Scan your python file with flake8
```sh
docker run jassouline/flake8:alpine my_python_file.py
```

<!-- ROADMAP -->
## Roadmap

See the [open issues](https://gitlab.com/devsecops-jordan-assouline-tools/flake8-docker-image/issues) for a list of proposed features (and known issues).

  
<!-- CONTRIBUTING -->

## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

  1. Fork the Project

2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)

3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)

4. Push to the Branch (`git push origin feature/AmazingFeature`)

5. Open a Pull Request

  <!-- LICENSE -->

## License
Distributed under the MIT License. See `LICENSE` for more information.

  
  
  <!-- CONTACT -->

## Contact

  

ASSOULINE Jordan - [Linkedin](https://www.linkedin.com/in/jordan-assouline-5b4aa17a/)

  Project Link: [https://gitlab.com/devsecops-jordan-assouline-tools/flake8-docker-image](https://gitlab.com/devsecops-jordan-assouline-tools/flake8-docker-image)

  
<!-- MARKDOWN LINKS & IMAGES -->

<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->

[contributors-shield]: https://img.shields.io/github/contributors/othneildrew/Best-README-Template.svg?style=flat-square

[contributors-url]: https://gitlab.com/devsecops-jordan-assouline-tools/flake8-docker-image/-/graphs/main

[forks-shield]: https://img.shields.io/github/forks/othneildrew/Best-README-Template.svg?style=flat-square

[forks-url]: https://gitlab.com/devsecops-jordan-assouline-tools/flake8-docker-image/-/project_members

[issues-shield]: https://img.shields.io/github/issues/othneildrew/Best-README-Template.svg?style=flat-square

[issues-url]: https://gitlab.com/devsecops-jordan-assouline-tools/flake8-docker-image/issues

[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=flat-square

[license-url]: https://gitlab.com/devsecops-jordan-assouline-tools/flake8-docker-image/LICENSE.txt

[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=flat-square&logo=linkedin&colorB=555

[linkedin-url]: https://www.linkedin.com/in/jordan-assouline-5b4aa17a/